import { expect } from "chai"
import sayHello from "../src/index"

describe(`Main File: index.js`, () => 
  describe(`sayHello`, () =>
    it(`should say "Hello guys!"`, () => {
      const str = sayHello();
      expect(str).to.equal("Hello guys!")
    })
  )
)