# ES6 Starting Pack

Simplest configuration I've found to code in ES6

## Get Started

1. Fork
2. Clone locally
3. Run `npm install` to install all dependencies
4. Run `npm run compile` to create the first batch of final files
5. Run `npm start` to begin editing

## Explanation

This project compiles ES6 into ES5 through [Babel](//babeljs.io).

It also has a test system ready through [Mocha](https://mochajs.org/) and [Chai](https://www.chaijs.com/).

### Available commands

```sh
> npm start
```

Compiles the files in `./src` to ES6 and runs them. Any file modified in `./src` will trigger a refresh automatically

-----

```sh
> npm run compile
```

Compiles the files in `./src` to ES6 and stores them in `./dist`. Additionally, `npm run compile:clean` removes the ./dist directory, and `npm run compile:build` creates the files. `npm run compile` does both (removes the directory *then* creates the files).

-----

```sh
> npm run production
```

Simply runs the files in `./dist`. If they don't exist (because you haven't run `npm run compile` first), you will get an error. There's also the `production:debug`, which will open a debug interface.

-----

```sh
> npm test
```

Runs the tests in `./test` through the Mocha test runner. Chai is used for the `expect` interface


### Files

- `./src`: contains the ES6 source files. Those files are not useful for production, only during development
- `./dist`: contains the ES5 compiled files. This directory is not checked into git, and is supposed to be recreated by developpers by running `npm run compile`.
- `./test`: contains tests to run with `npm test`.
- `.gitignore`: nothing of interest here, other than the fact that it ignores the `./dist` directory. This directory should be recreated locally, so there's no reason to check it into git
- `.npmignore`: contrary to `.gitignore`, `.npmignore` ignores `./src` and checks in `./dist`. If we were to publish this module on npm, we would want it to use the compiled ES5 code, which people can use, and there's no reason to send the ES6 code there.
- `package-lock.json`:  stores an exact, versioned dependency tree rather than using starred versioning like package.json itself (e.g. 1.0.*). This means you can guarantee the dependencies for other developers or prod releases. This is used by `npm`; if you use `yarn`, a `yarn.lock` will be generated. 
- `package.json`: regular package.json file, save for the fact that it contains a `babel` key to store the Babel configuration.

## License

Copyright (c) 2018, Jad Sarout

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Source: http://opensource.org/licenses/ISC

